\version "2.19.83"

#(set! paper-alist (cons '("my size" . (cons (* 8 in) (* 0.8 in))) paper-alist))

\header {
  tagline = ""
} 

\paper {
  #(set-paper-size "my size")
}

\layout {
    indent = 0.0\cm
    line-width = 20\cm 
    \context {
      \Staff
      \remove "Time_signature_engraver"
      \remove "Bar_engraver"
      \hide Stem
      \override TextScript.staff-padding = #1
    }
}
\new Staff <<
  \accidentalStyle dodecaphonic
\relative c' {
  a4^\markup{ \center-align{+0}}
  s4
  b4^\markup{ \center-align{+4}}
  s4
  cis4^\markup{ \center-align{-14}}
  s4
  dih4^\markup{ \center-align{+1}}
  s4
  e4^\markup{ \center-align{+2}}
  s4
  fih4^\markup{ \center-align{-9}}
  s4
  geh4^\markup{ \center-align{+19}}
  s4
  gis4^\markup{ \center-align{-12}}
}
>>