\version "2.19.83"

#(set! paper-alist (cons '("my size" . (cons (* 8 in) (* 0.8 in))) paper-alist))

\header {
  tagline = ""
} 

\paper {
  #(set-paper-size "my size")
}

\layout {
    indent = 0.0\cm
    line-width = 20\cm 
    \context {
      \Staff
      \remove "Time_signature_engraver"
      \remove "Bar_engraver"
      \hide Stem
      \override TextScript.staff-padding = #1
    }
}
\new Staff <<
  \accidentalStyle dodecaphonic
  %\transpose a c'
  %\transposePitchClasses {a b cis dih e fih geh gis} {a b cis dis e f g gis}
\relative c' {
  c4^\markup{ \center-align{+0}}
  s4
  d4^\markup{ \center-align{+4}}
  s4
  e4^\markup{ \center-align{-14}}
  s4
  fis4^\markup{ \center-align{-49}}
  s4
  g4^\markup{ \center-align{+2}}
  s4
  aes4^\markup{ \center-align{+41}}
  s4
  bes4^\markup{ \center-align{-31}}
  s4
  b4^\markup{ \center-align{-12}}
}
>>