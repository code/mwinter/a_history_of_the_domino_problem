\version "2.19.83"

#(set! paper-alist (cons '("my size" . (cons (* 8 in) (* 0.75 in))) paper-alist))

\header {
  tagline = ""
} 

\paper {
  #(set-paper-size "my size")
}

\layout {
    indent = 0.0\cm
    line-width = 20\cm 
    \context {
      \Staff
      \remove "Time_signature_engraver"
      \remove "Bar_engraver"
      \hide Stem
      \override TextScript.staff-padding = #1
    }
}
\new Staff <<
  \accidentalStyle dodecaphonic
\relative c' {
  c4^\markup{ \center-align{+0}}
  s4
  cis4^\markup{ \center-align{+5}}
  s4
  d4^\markup{ \center-align{+1}}
  s4
  dis4^\markup{ \center-align{+16}}
  s4
  fih4^\markup{ \center-align{+1}}
  s4
  g4^\markup{ \center-align{+2}}
  s4
  aih4^\markup{ \center-align{+19}}
}
>>