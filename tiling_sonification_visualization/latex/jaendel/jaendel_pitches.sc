(
((0..15) * 2 + 1).reverse.postln.collect({arg harm;
	var actual, quant, dev;
	actual = ((55 * harm.postln).cpsmidi % 12);
	quant = ((55 * harm.postln).cpsmidi % 12).round(0.5);
	dev = ((actual - quant) * 100).round.asInteger;
	["c", "cih", "cis", "deh", "d", "dih", "dis", "eeh", "e", "eih", "f", "fih", "fis", "geh", "g", "gih", "gis", "aeh", "a", "aih", "ais", "beh", "b", "bih", "c"][(((55 * harm.postln).cpsmidi % 12).postln.round(0.5) * 2).postln] ++ [",,", ",", "", "'", "''", "'''", "''''"][((55 * harm).cpsmidi / 12).trunc.asInteger - 2] ++ "4" ++ "_\\markup{" ++ harm.asString ++ "}" ++ "^\\markup{ \\center-align{" ++ if(dev >= 0, {"+"}, {""}) ++ dev.asString ++ "}}" ++ "\ns4\n"}).join
)