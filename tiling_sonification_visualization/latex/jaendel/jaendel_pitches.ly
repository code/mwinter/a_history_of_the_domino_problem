\version "2.19.83"

#(set! paper-alist (cons '("my size" . (cons (* 9 in) (* 1.75 in))) paper-alist))

\header {
  tagline = ""
} 

\paper {
  #(set-paper-size "my size")
}

\layout {
    indent = 0.0\cm
    line-width = 20\cm 
    \context {
      \Staff
      \remove "Time_signature_engraver"
      \remove "Bar_engraver"
      \hide Stem
      \override TextScript.staff-padding = #7
    }
}
\new Staff <<
  \accidentalStyle dodecaphonic
{
aeh'''4_\markup{31}^\markup{ \center-align{-5}}
s4
gih'''4_\markup{29}^\markup{ \center-align{-20}}
s4
fis'''4_\markup{27}^\markup{ \center-align{+6}}
s4
eih'''4_\markup{25}^\markup{ \center-align{+23}}
s4
eeh'''4_\markup{23}^\markup{ \center-align{-22}}
s4
deh'''4_\markup{21}^\markup{ \center-align{+21}}
s4
c'''4_\markup{19}^\markup{ \center-align{-2}}
s4
ais''4_\markup{17}^\markup{ \center-align{+5}}
s4
gis''4_\markup{15}^\markup{ \center-align{-12}}
s4
fih''4_\markup{13}^\markup{ \center-align{-9}}
s4
dih''4_\markup{11}^\markup{ \center-align{+1}}
s4
b'4_\markup{9}^\markup{ \center-align{+4}}
s4
geh'4_\markup{7}^\markup{ \center-align{+19}}
s4
cis'4_\markup{5}^\markup{ \center-align{-14}}
\clef bass
s4
e4_\markup{3}^\markup{ \center-align{+2}}
s4
a,,4_\markup{1}^\markup{ \center-align{+0}}
}
>>