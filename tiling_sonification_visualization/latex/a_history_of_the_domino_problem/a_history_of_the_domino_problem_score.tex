\documentclass[10pt]{letter}

\usepackage[a4paper, top=0.7in, bottom=0.7in, left=0.7in, right=0.7in]{geometry}
\usepackage{mathtools}
\usepackage{wasysym}
\usepackage{multicol}
\usepackage{dirtree}
\usepackage{underscore}
\usepackage{pdfpages}
\usepackage[framed,numbered]{sclang-prettifier}
\usepackage{listings}
\usepackage[obeyspaces]{url}
\usepackage{datetime2}
%\usepackage{draftwatermark}
\renewcommand{\arraystretch}{1.3}
\usepackage{graphicx}
\usepackage{enumitem}

\DTMsetdatestyle{default}
\DTMsetup{datesep={.}}

%\SetWatermarkColor[rgb]{1, 0.6, 0.6}
%\SetWatermarkScale{2}
%\SetWatermarkHorCenter{1.25in}
%\SetWatermarkVerCenter{1.25in}

% Define Language
\lstdefinelanguage{Lilypond}
{
  % list of keywords
  morekeywords={
  }
}
 
% Set Language
\lstset{
	numbers=left, 
	numberstyle=\small,
	numberstyle = \color{black!33},
	numbersep=8pt, 
	frame = single, 
  	language={Lilypond},
}

\newenvironment{note}{
\vspace{-3mm}
\small
\par
\leftskip=4em\rightskip=5em
\noindent\ignorespaces}{\par\smallskip}

\makeatletter
\newenvironment{thebibliography}[1]
     {\list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}
\newcommand\newblock{\hskip .11em\@plus.33em\@minus.07em}
\makeatother

\begin{document}

\textit{\textbf{a history of the domino problem}} \\
a performance-installation

\begin{flushright} 
michael winter \\ schloss solitude and cdmx; 2018 - 2019 \\
\end{flushright} 

\bigskip

\begin{center}
\begin{tabular}{cc}

  \centering
  \includegraphics[width=0.49\linewidth]{selects/maquina.png}

  \centering
  \includegraphics[width=0.49\linewidth]{selects/discos.png}
\end{tabular}
\end{center}

\bigskip

\textbf{Description / note}

The domino problem, first posed by Hao Wang in 1961, is an epistemological question that asks whether there exists an algorithm to determine if an arbitrary finite set of tiles with colored edges can cover the plane such that adjacent edges match color. He conjectured that if a set of such tiles covers the plane, it can only do so periodically. However, in 1966, his student, Robert Berger, proved that the problem is undecidable (that is, there is no general algorithm) by showing the existence of a set of tiles that can only cover the plane aperiodically. This initial set contained more than 20000 tiles. Over the past 60 years, there has been a continual reduction in the size of provably aperiodic sets to the most recent discovery of a set of 11 tiles along with a proof that no smaller sets exist. It is a beautiful narrative / history of a particular epistemological problem that challenged a group of people not only to solve it, but to understand it to the extent possible. 

\textit{a history of the domino problem} is a performance-installation that visualizes and sonifies aperiodic tilings in order to trace the history of the domino problem. The tilings are visualized using a cryptographic scheme in which two `shadow images', each which look completely random independently, are combined / overlayed at various orientations to reveal the tilings in a form that replaces the colored edges of the original constructions with binary codes. The shadow images are printed on photomasks typically used to manufacture computer chips: quartz wafers with a chrome coating etched at a pixel size of approximately 20 microns. A high-precision, motorized multiaxis stage aligns the shadow images to reveal the tilings (along with 3 other images of poetic texts inspired by the history of the domino problem). The whole apparatus rests on a light source that illuminates the photomasks which are then magnified and projected. The visualizations are accompanied by musical compositions generated from the tilings that can be realized live by performers as intermittent augmentations within the installation or as singular pieces in concert. 

The aim of the work is to create an artistic experience that demonstrates the aesthetic qualities of these found mathematical objects while also functioning as a sort of historical record.

This is ultimately a piece about how things fit together.

\bigskip

\textbf{Installation and performance setting}

As an installation, the apparatus that aligns the image should be centered in a dark room such that observers can view the photomasks up close. Ideally, this should be set up with a teleprompter mirror at a 45 degree angle above the apparatus so that the viewer can see the photomasks without having to bend over. On the other side of the mirror, a video camera is placed such that the resulting projected image aligns with what the viewer sees in the teleprompter mirror. The camera side of the mirror must be darkened out with a cover in order to allow the viewer to only see the reflection of the photomasks. The projection should be as large as possible and at as high a resolution as possible. Ideally, the camera should be able to zoom into the images of the tilings to show more detail.

In the installation, recordings of the musical pieces are played back; sometimes randomly and sometimes in sync with the respective tilings from which they were generated. The installation can be augmented (e.g. for an exhibition opening) by live performances of the musical pieces instead of the recordings. If so, direct access to the apparatus should be avoided in order for a situation where the observers can view the projected images and listen to the musical accompaniment in a tranquil and focused environment. 

A demo of the apparatus is available at: \url{https://vimeo.com/375784136}.

\bigskip

\textbf{Photomask alignment}

Between the two photomasks, there are nine embedded images which can be seen at nine precise orientation organized in a 3 x 3 grid. The image area is surrounded by Moire pattern and Vernier markings to aid in the alignment. Provided below are a description of each of these markings.

\begin{center}
  \includegraphics[width=0.7\linewidth]{selects/oraclesannotated.jpg}
  \end{center}
  
 \begin{description}[labelindent=0.5cm]
 \item [unidimensional Verniers:] These are the most useful markings for image alignment. For each axis, there are a set of coarse Verniers (bordering the image) and a set of fine Verniers (further from the image) which move 5 times the speed of the coarse Veniers. Every time an image is aligned the white blob will be centered in both the coarse and fine Verniers. These markings essentially amplify and scale the distance between the image (640 microns) and can be used by a motion tracker in a closed-loop alignment system.
 \item [multidimenional Verniers:] These are Verniers that are centered in a two-dimenional space everytime an image is focused.
 \item [linear Moire grating:] These gratings can be used to make sure that the plates are aligned rotationally (resulting in a completely monochrome bar without any patterns).
 \item [circular Moire grating:] These gratings best represent the grid of the images. When an image is aligned the respective grating on the grid will be dark. The number of fringes denotes the accuracy of the alignment (none means perfectly aligned).
 \end{description}


High precision optical stages are used for the alignment of the wafers. Ideally, the wafers are not touching (separated by a few microns). If the wafers touch, they will degrade over time as they move across each other. However, it is very difficult to achieve perfect alignment without the photomasks touching as the they need to be aligned in all 6 degrees of freedom ($x$, $y$, $z$, $\theta x$, $\theta y$, and $\theta z$) in order for the resulting image to be properly produced. Only the $x$ and $y$ axis need to be moved to find the images once all the other degrees of freedom are set accurately. 

The original setup is as follows. Each of the photomasks are mounted onto tilt stages to be able to align the masks together rotationally (note that a more ideal setup would use goniometer stages). One of the tilt / goniometer stages is then affixed to a stage with 3 degrees of freedom: $x$, $y$, and $z$. The other is fixed directly to an optical breadboard.

To automate the alignment, high precision motors are used to move one of the photomasks on the $x$ and $y$ axes. In the original setup, the high precision motors are stepper motors. If the photomasks are not touching, an open-loop system can be used to automate alignment. That is, the accuracy of the step count of the motors should be sufficient for alignment. However, if the photomasks need to touch in order to produce the resulting images (as is the often the case with the original setup), the friction between the two photomasks will cause inaccuracies in an open-loop system. To compensate for this, the Vernier markings can be tracked optically (using motion-tracking software or opto-interrupts) in order to create a closed-loop system. The software used to automate the system and control the motors is detailed in the following section.

\begin{center}
  \includegraphics[width=0.7\linewidth]{selects/maquinalit.jpg}
  \end{center}

\bigskip

\textbf{Overview of computer code}

As the code is subject to change / improvements, the current state of the code is available through a git repository at the following address: \url{https://gitea.unboundedpress.org/mwinter/a_history_of_the_domino_problem}. The respository contains the computer code needed to run the installation along with all the code that generated the musical pieces and all the code / schematics / files to rebuild the installation. Further, this document along with each of the scores is marked with the date it was generated in order to verify it is the most recent version.

The repository is organized by the various languages / formats used to generate the musical pieces and visualizations as well as control the installation. To start, I will focus on the software needed to control the installation which includes four basic components:

\begin{itemize}[labelindent=0.5cm]
\item A SuperCollider program with the filename \url{installation_control.scd}.

\item A GUI interface written using Open Stage Control with the filename \url{installation_control_gui.json}

\item A motion tracker written in Python using OpenCV to track the Verniers for a closed-loop system with the filename \url{vernier_tracker.py}

\item Arduino code to communicate between Supercollider and the stepper-motor drivers with the filename\\ \url{multistepper.ino}
\end{itemize}

In the original setup, the Arduino code is loaded onto the Arduino and the three other programs are loaded and launched on a computer (e.g., a Raspberry Pi) connected to the Arduino to control the system. The SuperCollider program is the main control center. Both the GUI and the motion tracker (which takes in video input from the video camera) communicate to the SuperCollider program through OSC messages, which, in turn, sends serial messages to the Arduino to control the stepper motors via the stepper motor drivers. The Arduino code not only sends messages to the stepper motor drivers, but also takes input signals from the limit switches of the motors to ensure that the motors are not destroyed by running into a hard stop. 

All the other code (primarily written in SuperCollider) was written to generate the musical pieces as well as the visualizations and is maintained in the repository for reference. For the musical pieces, the SuperCollider code generates electronic realizations of the compositions and Lilypond files for generation of the musical scores. 

The photomasks were printed from the GDSII file format (\url{*.gds}). The workflow consisted of \url{*.png} generated by SuperCollider and then formatted into \url{*.gds} files using the Python KLayout API. However, all that is needed to reprint the photomasks are the files \url{wafer_1.gds} and \url{wafer_2.gds}. Note that the the \url{*.gds} files along with all larger files are archived in a code releases available at \url{https://gitea.unboundedpress.org/mwinter/a_history_of_the_domino_problem/releases}. Again, all the other files in the repository are maintained for reference.

\bigskip

\textbf{Appendix: partial historical timeline of the domino problem, selected bibliography, acknowledgments, and tiling images}

pre-history:\\
\begin{tabular}{p{0.8in}p{1.25in}p{4.25in}}
17th century &	Leibniz & pioneer of binary arithmetic and the idea of computing machines \\
ca 1928 	& Hilbert & posed the original "Entsheidungsproblem" \\
ca 1931 & Goedel & first showed that their exists truths that are undecidable with a finite set of axioms \\
ca 1936 & Turing & invented the concept of the modern day computer and showed its limits yet was unfortunately persecuted for his sexuality despite being a key figure in the triumph of the allied nations against the nazi regime
\end{tabular}

conjecture and first proof:\\
\begin{tabular}{p{0.8in}p{1.25in}p{4.25in}}
ca 1961	& Wang & conjectured that aperiodic tilings of the plane did not exist \\
ca 1966 & Berger & showed that an aperiodic set 20000+ tiles exist (using his method this was quickly reduced to 104 then 92 by Berger and Knuth, respectively)
\end{tabular}

first wave of reduction:\\
\begin{tabular}{p{0.8in}p{1.25in}p{4.25in}}
ca 1971	& Robinson \& Lauchli & 56 and 40 tiles, respectively; using a similar technique of tiling arbitrarily large squares discovered independently
\end{tabular}

second wave of reduction:\\
\begin{tabular}{p{0.8in}p{1.25in}p{4.25in}}
ca 1986	& Penrose \& Amman & 32 and 16 tiles, respectively; using a method that translates different, non-squared aperiodic tiles into Wang tiles
\end{tabular}

third wave of reduction:\\
\begin{tabular}{p{0.8in}p{1.25in}p{4.25in}}
ca 1996 & Kari \& Culik & 13 tiles using an new construction with aperiodic integer sequences
\end{tabular}

final reduction\\
\begin{tabular}{p{0.8in}p{1.25in}p{4.25in}}
ca 2015	& Jaendel \& Rao & 11 tiles with an incredible computer-assisted proof that no smaller aperiodic sets exist
\end{tabular}

\bigskip

\nocite{*}
\bibliographystyle{unsrt}
\bibliography{hdp}

\bigskip

A special thanks to: Alice Koegel, Ana Filipovic\textsuperscript{$*$}, Angela Butterstein\textsuperscript{†}, Anita Carey-Yard\textsuperscript{†}, Aykan Safoğlu\textsuperscript{$*$}, Bjoern Gottstein, Charis von Ristok\textsuperscript{†}, Christof Pruss, Daniela Kern-Michler\textsuperscript{§}, David Frühauf\textsuperscript{$*$}, David Mathews\textsuperscript{$*$}, Deborah Walker, Denise Helene Sumi\textsuperscript{$*$}, Didier Aschour, Edith Lázár\textsuperscript{$*$}, Elena Morena Weber\textsuperscript{$*$}, Elke aus dem Moore\textsuperscript{†}, Elmar Mellert, Florian Hoelscher, Gaetan Borot, Helmut Dietz\textsuperscript{†}, Irasema Fernandez, Johanna Markert\textsuperscript{$*$}, Julian Hartbaum\textsuperscript{‡}, Konstantin Lom\textsuperscript{†}, Leon Müllner\textsuperscript{$*$}, Luise Boege\textsuperscript{$*$}, Lukas Ludwig\textsuperscript{$*$}, Luke Wilkins\textsuperscript{$*$}, Marieanne Roth\textsuperscript{†}, Mathias Irmsher\textsuperscript{‡}, Mitra Wakil\textsuperscript{$*$}, Patrizia Bach\textsuperscript{$*$}, Philip Mecke\textsuperscript{$*$}, Robert Blatt\textsuperscript{$*$}, Sander Wickersheim\textsuperscript{†}, Savyon\textsuperscript{$*$}, Silke Pflüger\textsuperscript{†}, Sílvia das Fadas\textsuperscript{$*$}, Simar Preet Kaur\textsuperscript{$*$}, Sonja Flury\textsuperscript{†}, Sophia Guggenberger\textsuperscript{$*$}, Sophie-Charlotte Thieroff\textsuperscript{†}, Stephan Martens\textsuperscript{‡}, Susanna Flock\textsuperscript{$*$}, Tom Rosenberg\textsuperscript{$*$}, Vincenzo Talluto\textsuperscript{§}, and Yuval Shenhar\textsuperscript{$*$}.

* denotes contemporary resident at Akademie Schloss Solitude\\
† denotes Akademie Schloss Solitude staff\\
‡ denotes Institut für Mikroelektronik Stuttgart staff\\
§ denotes Newport Optics staff\\



\vspace{\fill}

\begin{flushright} 
version generated: \today
\end{flushright} 

\newpage
\vspace*{\fill}
  \centering
  \includegraphics[width=1\linewidth]{selects/berger.jpg}
Berger
\vspace*{\fill}

\newpage
\vspace*{\fill}
  \centering
  \includegraphics[width=1\linewidth]{selects/robinson.jpg}
Robinson
\vspace*{\fill}

\newpage
\vspace*{\fill}
  \centering
  \includegraphics[width=1\linewidth]{selects/penrose.jpg}
Penrose
\vspace*{\fill}

\newpage
\vspace*{\fill}
  \centering
  \includegraphics[width=1\linewidth]{selects/ammann.jpg}
Ammann
\vspace*{\fill}

\newpage
\vspace*{\fill}
  \centering
  \includegraphics[width=1\linewidth]{selects/kari.jpg}
Kari
\vspace*{\fill}

\newpage
\vspace*{\fill}
  \centering
  \includegraphics[width=1\linewidth]{selects/jaendel.jpg}
Jaendel-Rao

\vspace*{\fill}

\includepdf[pages={-}]{../berger/berger\string_score.pdf}
\includepdf[pages={-}]{../robinson/robinson\string_score.pdf}
\includepdf[pages={-}]{../penrose/penrose\string_score.pdf}
\includepdf[pages={-}]{../ammann/ammann\string_score.pdf}
\includepdf[pages={-}]{../kari/kari\string_score.pdf}
\includepdf[pages={-}]{../jaendel/jaendel\string_rao\string_score.pdf}



\end{document}