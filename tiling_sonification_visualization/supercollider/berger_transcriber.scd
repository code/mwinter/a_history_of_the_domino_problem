(
~bergerTranscribe = {arg seqs;
	var dir, basePath;

	//thisThread.randSeed = 100;

	dir = thisProcess.nowExecutingPath.dirname;
	basePath = dir +/+ ".." +/+ "score" +/+ "lilypond" +/+ "berger_knuth";

	seqs.do({arg part, p;

		var lilyFile, lilyString, lastNote, lastTimeSig, lastClef, lastDynamic, measureChange, measureIndex;

		//create file
		lilyFile = File(basePath +/+ "includes" +/+ "berger_" ++ "part_" ++ p.asString ++ ".ly".standardizePath,"w");

		//start lilypond directives
		lilyString = "";
		lilyString = lilyString ++ "{\n\\set tupletFullLength = ##t\n\\set Score.markFormatter = #format-mark-box-numbers\n";
		lilyString = lilyString + "\\tempo 4 = 60-90 \n  \\mark \\default \n";

		lastNote = "r";
		lastTimeSig = "";
		//lastClef = "\\clef treble ";
		lastClef = if((55 * part.flop[1][1]).cpsmidi >= 60, {"\\clef treble "}, {"\\clef bass "});
		lilyString = lilyString ++ lastClef ++ "\n";
		lastDynamic = "";
		measureChange = true;
		measureIndex = 0;

		part.flop.size.do({arg nIndex;
			var dirs, dur, sustain, harm, amp, quant1, quant2, timeStamp, measureStart, measureDur1, measureDur2,
			note, beatCount, durUnit, timeSig, clef, tuplet1, tuplet2, dynamic, insertTS;

			dirs = part.flop[nIndex];
			dur = dirs[0];
			//sustain = dirs[1, 1]; //not used
			harm = if(nIndex == 0, {0}, {dirs[1]});
			amp = if(nIndex == 0, {0}, {dirs[2]});
			timeStamp = dirs[3];
			measureStart = dirs[4];
			measureDur1 =  dirs[5];
			measureDur2 =  dirs[6];
			quant1 = dirs[7];
			quant2 = dirs[8];

			beatCount = timeStamp;
			durUnit = 0.25;

			insertTS = {
				lilyString = lilyString ++ " | " ++ "%{ measure: " ++ measureIndex.asString ++ " beat: " ++ beatCount ++ " %} ";
				if((measureIndex % 32 == 0) && (measureIndex != 0), {lilyString = lilyString ++ " \\bar \"||\" \\mark \\default "});
				timeSig = "\\numericTimeSignature \\time " ++ if((measureDur2 / 0.5 % 2) == 0, {measureDur2.trunc.asInteger.asString ++ "/4 "},
					{(measureDur2 / 0.5).trunc.asInteger.asString ++ "/8 "});
				if(lastTimeSig != timeSig, {lilyString = lilyString ++ timeSig}, {lilyString = lilyString ++ "%{" ++ timeSig ++ "%} "});
				measureIndex = measureIndex + 1;
				lastTimeSig = timeSig;
			};

			/*
			if((amp != 0) && (nIndex != (part.flop.size - 1)), {
				note = ["c", "cis", "d", "dis", "e", "f", "fis", "g", "gis", "a", "ais", "b"][((55 * harm).cpsmidi % 12).trunc.asInteger];
				note = note ++ [",,", ",", "", "'", "''"][((55 * harm).cpsmidi / 12).trunc.asInteger - 2];
			}, {note = "r"});
			*/

			if((amp != 0) && (nIndex != (part.flop.size - 1)), {
				note = ["c", "cih", "cis", "deh", "d", "dih", "dis", "eeh", "e", "eih", "f", "fih", "fis", "geh", "g", "gih", "gis", "aeh", "a", "aih", "ais", "beh", "b", "bih"][((55 * harm).cpsmidi % 12).round(0.5) * 2];
				note = note ++ [",,", ",", "", "'", "''", "'''", "''''"][((55 * harm).cpsmidi / 12).trunc.asInteger - 2];
			}, {note = "r"});


			dynamic = case
			{amp == 0.0} {""}
			{amp == (0.4/3 + 0.002)} {"\\p "}
			{amp == (0.6/3 + 0.002)} {"\\mp "}
			{amp == (0.8/3 + 0.002)} {"\\mf "}
			{amp == (0.4/2 + 0.001)} {"\\p "}
			{amp == (0.6/2 + 0.001)} {"\\mp "}
			{amp == (0.8/2 + 0.001)} {"\\mf "}
			{amp == 0.4} {"\\p "}
			{amp == 0.6} {"\\mp "}
			{amp == 0.8} {"\\mf "};

			tuplet1 = case
			{quant1 == 0.25} {[1/4, " "]}
			{quant1 == (1/3)} {[1/6, "3\/2 "]}
			{quant1 == (1/5)} {[1/5, "5\/4 "]};

			tuplet2 = case
			{quant2 == 0.25} {[1/4, " "]}
			{quant2 == (1/3)} {[1/6, "3\/2 "]}
			{quant2 == (1/5)} {[1/5, "5\/4 "]};

			//initial rest
			if(nIndex == 0, {
				insertTS.value;
				dur.trunc.asInteger.do({
					lilyString = lilyString ++ note ++ "4 "
				});
				if((dur % 0.25) != 0, {lilyString = lilyString ++ "\\tuplet " ++ tuplet1[1] + " { "});
				if((dur % 1) != 0, {
					lilyString = lilyString ++ note ++ switch(((dur % 1) / tuplet1[0]).round.asInteger, 1, {"16 "}, 2, {"8 "}, 3, {"8. "}, 4, {"4 "});
				});
			},
			{
				var noteFrac1, noteFrac2, quarters;

				//put clef if necessary
				/*
				if(amp != 0, {
					clef = if((55 * harm).cpsmidi >= 60, {"\\clef treble "}, {"\\clef bass "});
					if(lastClef != clef, {lilyString = lilyString + clef});
					lastClef = clef;
				});
				*/

				noteFrac1 = timeStamp.ceil.asInteger - timeStamp;
				if(noteFrac1 > 0, {
					lilyString = lilyString ++ note ++ switch((noteFrac1 / tuplet1[0]).round.asInteger, 1, {"16 "}, 2, {"8 "}, 3, {"8. "}, 4, {"4 "});
					lilyString = lilyString ++ if(lastDynamic != dynamic, {dynamic}, {""});
					lastDynamic = dynamic;
					if(noteFrac1 != dur, {lilyString = lilyString ++ "~ "});
					if((noteFrac1 % 0.25) != 0, {lilyString = lilyString ++ " } "});
					beatCount = beatCount + noteFrac1;
					if(beatCount == (measureStart + measureDur1), {insertTS.value});
				});

				noteFrac2 = (timeStamp + dur) % 1;
				if((noteFrac2 > 0) && (nIndex == (part.flop.size - 1)), {noteFrac2 = noteFrac2.trunc.asInteger});

				quarters = (dur - noteFrac1).trunc.asInteger;
				quarters.do({arg q;
					lilyString = lilyString ++ note ++ "4 ";
					if((q != (quarters - 1)) || (noteFrac2 > 0),  {lilyString = lilyString ++ "~ "});
					beatCount = beatCount + 1;
					if(beatCount == (measureStart + measureDur1), {insertTS.value});
				});

				if(noteFrac2 > 0, {
					if((noteFrac2 % 0.25) != 0, {[noteFrac2, tuplet2]; lilyString = lilyString ++ "\\tuplet " ++ tuplet2[1] + " { "});
					lilyString = lilyString ++ note ++ switch((noteFrac2 / tuplet2[0]).round.asInteger, 1, {"16 "}, 2, {"8 "}, 3, {"8. "}, 4, {"4 "});
					beatCount = beatCount + noteFrac2;
					if(beatCount == (measureStart + measureDur1), {insertTS.value});
				});

				lilyString = lilyString ++ "%{notechange%} ";


				if((noteFrac2 > 0) && (nIndex == (part.flop.size - 1)), {
					lilyString = lilyString ++ "r" ++ switch(((1 - noteFrac2) / tuplet2[0]).round.asInteger, 1, {"16 "}, 2, {"8 "}, 3, {"8. "}, 4, {"4 "});
					if((noteFrac2 % 0.25) != 0, {lilyString = lilyString ++ " } "});
					beatCount = beatCount + (1 - noteFrac2);
				});

				if((nIndex == (part.flop.size - 1)), {
					((measureStart + measureDur1) - beatCount).do({
						lilyString = lilyString ++ "r" ++ "4 "
					});
				});
			}
			);

		});

		lilyString.findRegexp(
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4 (\\\\mf |\\\\mp |\\\\p |)(~ )*" ++
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4 (\\\\mf |\\\\mp |\\\\p |)(~ )*" ++
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4 (\\\\mf |\\\\mp |\\\\p |)(~ )*" ++
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4").clump(15).do({arg match;
			if(
				[

					match[1][1] ++ match[2][1],
					match[5][1] ++ match[6][1],
					match[9][1] ++ match[10][1],
					match[13][1] ++ match[14][1]
				].asSet.size == 1, {lilyString = lilyString.replace(match[0][1], match[1][1] ++ match[2][1] ++ "1" ++ match[3][1])
			})
		});

		lilyString.findRegexp(
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4 (\\\\mf |\\\\mp |\\\\p |)(~ )*" ++
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4 (\\\\mf |\\\\mp |\\\\p |)(~ )*" ++
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4").clump(11).do({arg match;
			if(
				[

					match[1][1] ++ match[2][1],
					match[5][1] ++ match[6][1],
					match[9][1] ++ match[10][1]
				].asSet.size == 1, {lilyString = lilyString.replace(match[0][1], match[1][1] ++ match[2][1] ++ "2." ++ match[3][1])
			})
		});

		lilyString.findRegexp(
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4 (\\\\mf |\\\\mp |\\\\p |)(~ )*" ++
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4").clump(7).do({arg match;
			if(
				[

					match[1][1] ++ match[2][1],
					match[5][1] ++ match[6][1]
				].asSet.size == 1, {lilyString = lilyString.replace(match[0][1], match[1][1] ++ match[2][1] ++ "2" ++ match[3][1])
			})
		});

		lilyString.findRegexp(
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)8 (\\\\mf |\\\\mp |\\\\p |)(~ )*" ++
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)8").clump(7).do({arg match;
			if(
				[

					match[1][1] ++ match[2][1],
					match[5][1] ++ match[6][1]
				].asSet.size == 1, {lilyString = lilyString.replace(match[0][1], match[1][1] ++ match[2][1] ++ "4" ++ match[3][1])
			})
		});

		lilyString.findRegexp(
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4 (\\\\mf |\\\\mp |\\\\p |)(~ )*" ++
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)8 ").clump(7).do({arg match;
			if(
				[

					match[1][1] ++ match[2][1],
					match[5][1] ++ match[6][1]
				].asSet.size == 1, {lilyString = lilyString.replace(match[0][1], match[1][1] ++ match[2][1] ++ "4. " ++ match[3][1])
			})
		});

		lilyString.findRegexp(
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)8 (\\\\mf |\\\\mp |\\\\p |)(~ )*" ++
			"([a-g]|[a-g]+is|[a-g]+ih|[a-g]+eh|r)(,,|,|'|''|)4 ").clump(7).do({arg match;
			if(
				[

					match[1][1] ++ match[2][1],
					match[5][1] ++ match[6][1]
				].asSet.size == 1, {lilyString = lilyString.replace(match[0][1], match[1][1] ++ match[2][1] ++ "4. " ++ match[3][1])
			})
		});


		//write file
		lilyString = lilyString ++ "\\bar \"|.\"\n}";
		lilyFile.write(lilyString);
		lilyFile.close;
	});

};

)