(
~robinsonTranscribe = {arg musicData;
	var dir, basePath, subTiling, secs;

	dir = thisProcess.nowExecutingPath.dirname;
	basePath = dir +/+ ".." +/+ "score" +/+ "lilypond" +/+ "robinson";

	secs = musicData[1].integrate[..(musicData[1].size - 2)].reverse;


	//runs twice for all parts or just instrumental parts
	musicData[0].do({arg part, p;
		var amps, harm, modi, lilyFile, lilyString, voices, lastVal, lilyNotes, lilyOcts, lilyDynamic, lastDynamic,
		lilyNote, lilyDur, lilyRest, curTime = 0, noteTuples;

		//this combines rows to make the note information for each part
		//in the piece, a part are combined subsets of 4 rows spaced 16 rows apart
		//the instrumentalists are all odd harmonics the rest is filled in by the electronics
		amps = [];
		part.do({arg data;
			if((data[0] == 0).not, {
				amps = amps ++ (data[0] * 0.25 * 4).asInteger.collect({[data[1], data[2], data[3]]});
			});
		});

		//create file
		lilyFile = File(basePath +/+ "includes" +/+ "robinson_" ++ "part_" ++ (p + 1).asString ++ ".ly".standardizePath,"w");

		//start lilypond directives
		lilyString = "";

		lastVal = nil;
		lastDynamic = nil;

		//start voice
		lilyString = lilyString ++ "\n{ ";
		lilyString = lilyString ++ "\n\\set Score.markFormatter = #format-mark-box-numbers ";
		lilyString = lilyString + "\\tempo 4 = 60-90 \n";
		lilyString = lilyString + "\\time 4/4\n \\mark \\default \n";

		lilyNotes = ["c", "cih", "cis", "deh", "d", "dih", "dis", "eeh", "e", "eih", "f", "fih", "fis", "geh", "g", "gih", "gis", "aeh", "a", "aih", "ais", "beh", "b", "bih"];
		lilyOcts = [",,", ",", "", "'", "''", "'''", "''''"];

		amps.clump(4).do({arg beat, i;
			var gSum = 0;
			beat.separate({arg a, b; a != b}).do({arg group, g; var noteLength, target = 0;
				noteLength = group.size;
				gSum = gSum + noteLength;

				//add ties
				lilyString = lilyString ++ if((group[0][0] == lastVal) && (group[0][1] != 0), {"~ "}, {""});
				//add barcheck count
				lilyString = lilyString ++ if((curTime % 4 == 0) && (i != 0), {"| "}, {""});

				if(curTime / 4 == secs.last, {secs.pop; lilyString = lilyString ++ " \\bar \"||\" \\mark \\default "});

				//note if not
				lilyNote = lilyNotes[((12.midicps * group[0][0]).cpsmidi.round(0.5) % 12) * 2];
				lilyNote = lilyNote ++ lilyOcts[((12.midicps * group[0][0]).cpsmidi.round(0.5) / 12).asInteger - 2];
				if(group[0][1] == 0, {lilyNote = "r"});
				//duration
				lilyDur = switch(noteLength, 1, {"16 "}, 2, {"8 "}, 3, {"8. "}, 4, {"4 "});
				lilyRest = "";

				//add dynamic
				lilyDynamic = if((group[0][1] == 0) || (group[0][2] == lastDynamic), {""}, {["\\paren\\f ", "\\paren\\mf ", "\\paren\\mp ", "\\paren\\p "][group[0][2]]});

				//total directive
				lilyString = lilyString ++ lilyNote ++ lilyDur ++ lilyDynamic ++ lilyRest;
				//beam group
				if((g == 0) && (noteLength != 4), {lilyString = lilyString ++ " [ "});
				if((gSum == 4) && (noteLength != 4), {lilyString = lilyString ++ " ] "});

				lastVal = group[0][0];
				lastDynamic = if((group[0][1] == 0), {0}, {group[0][2]});
				curTime = curTime + (noteLength / 4);
			});
		});

		//end voice
		lilyString = lilyString ++ " ] \\bar \"|.\" } \n";



		noteTuples = [lilyNotes, lilyOcts].allTuples.collect({arg val; val.join}).join("|");

		lilyString.findRegexp("(" ++ noteTuples ++ ")4 \\\\paren(\\\\f|\\\\mf|\\\\mp|\\\\p) ~ (" ++ noteTuples ++ ")4 ~ (" ++ noteTuples ++ ")4 ~ (" ++ noteTuples ++ ")4").clump(6).do({arg match;
			lilyString = lilyString.replace(match[0][1], match[1][1] ++ "1 \\paren" ++ match[2][1])});

		lilyString.findRegexp("(" ++ noteTuples ++ ")4 \\\\paren(\\\\f|\\\\mf|\\\\mp|\\\\p) ~ (" ++ noteTuples ++ ")4 ~ (" ++ noteTuples ++ ")4").clump(5).do({arg match;
			lilyString = lilyString.replace(match[0][1], match[1][1] ++ "2. \\paren" ++ match[2][1])});

		lilyString.findRegexp("(" ++ noteTuples ++ ")4 \\\\paren(\\\\f|\\\\mf|\\\\mp|\\\\p) ~ (" ++ noteTuples ++ ")4").clump(4).do({arg match;
			lilyString = lilyString.replace(match[0][1], match[1][1] ++ "2 \\paren" ++ match[2][1])});



		//consolidate notes
		lilyString.findRegexp("(" ++ noteTuples ++ ")4 ~ (" ++ noteTuples ++ ")4 ~ (" ++ noteTuples ++ ")4 ~ (" ++ noteTuples ++ ")4").clump(5).do({arg match;
			lilyString = lilyString.replace(match[0][1], match[1][1] ++ "1")});

		lilyString.findRegexp("(" ++ noteTuples ++ ")4 ~ (" ++ noteTuples ++ ")4 ~ (" ++ noteTuples ++ ")4").clump(4).do({arg match;
			lilyString = lilyString.replace(match[0][1], match[1][1] ++ "2.")});

		lilyString.findRegexp("(" ++ noteTuples ++ ")4 ~ (" ++ noteTuples ++ ")4").clump(3).do({arg match;
			lilyString = lilyString.replace(match[0][1], match[1][1] ++ "2")});


		//consolidate rests
		lilyString.findRegexp("r4 r4 r4 r4").clump(2).do({arg match;
			lilyString = lilyString.replace(match[0][1], "R1")});

		lilyString.findRegexp("r4 r4 r4").clump(2).do({arg match;
			lilyString = lilyString.replace(match[0][1], "r2.")});

		lilyString.findRegexp("r4 r4").clump(2).do({arg match;
			lilyString = lilyString.replace(match[0][1], "r2")});

		//write file
		lilyFile.write(lilyString);
		lilyFile.close;
	});

};
)
