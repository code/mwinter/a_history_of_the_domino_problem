(
~kariTranscribe = {arg seqs;
	var dir, basePath, subTiling, tileMap, tiles, genTileMap, odds, harms, amps;

	dir = thisProcess.nowExecutingPath.dirname;
	basePath = dir +/+ ".." +/+ "score" +/+ "lilypond" +/+ "kari_culik";

	seqs.do({arg part, p;

		var lilyFile, lilyString, lastNote, lastTimeSig, lastClef, lastDynamic, measureIndex, beatCountTotal;

		//create file
		lilyFile = case
		{p == 0} {File(basePath +/+ "includes" +/+ "kari_culik_perc_" ++ "part" ++ ".ly".standardizePath,"w")}
		{p == 1} {File(basePath +/+ "includes" +/+ "kari_culik_bass_" ++ "part" ++ ".ly".standardizePath,"w")}
		{p > 1} {File(basePath +/+ "includes" +/+ "kari_culik_ensemble_" ++ "part_" ++ (p - 2).asString ++ ".ly".standardizePath,"w")};

		//start lilypond directives
		lilyString = "";
		lilyString = lilyString ++ "{\n\\set tupletFullLength = ##t\n\\set Score.markFormatter = #format-mark-box-numbers\n";
		lilyString = lilyString + "\\tempo 4 = 40-50 \n  \\mark \\default \n";
		lilyString = lilyString ++ case
		{p == 0} {"\\clef percussion "}
		{p == 1} {"\\clef bass "}
		{p > 1} {"\\clef treble "};

		lastNote = "R";
		lastTimeSig = "";
		lastClef = if(p == 1, {"\\clef bass "}, {"\\clef treble "});
		lastDynamic = "";
		measureIndex = 0;
		beatCountTotal = 0;
		part.size.do({arg nIndex;
			var pIndex, measureDurs, note, dur, pIndexMod;
			pIndex = part[nIndex][0];
			measureDurs = part[nIndex][2];

			pIndexMod = case
			{p == 0} {[59, 0, 62][pIndex]}
			{p == 1} {[0, 36, 43][pIndex]}
			{p > 1} {(60 + (pIndex * 7))};
			note = ["c", "cis", "d", "dis", "e", "f", "fis", "g", "gis", "a", "ais", "b"][pIndexMod % 12];
			note = note ++ [",,", ",", "", "'", "''", "'''", "''''"][(pIndexMod / 12).trunc.asInteger - 2];
			if((pIndexMod == 0) || (pIndexMod == 95), {note = "R"});
			if((p == 0) && (pIndexMod == 59), {note = "\\stemDown " ++ note});
			if((p == 0) && (pIndexMod == 62), {note = "\\stemUp " ++ note});

			measureDurs.do({arg measureDur, m;
				var timeSigMult, timeSig;

				if((measureIndex % 32) == 0, {lilyString = lilyString ++ " \\bar \"||\" \\mark \\default "});
				if(nIndex != 0, {lilyString = lilyString ++ " | " ++ "%{ noteIndex: " ++ nIndex.asString ++ " beat: " ++ beatCountTotal ++ " %} "});

				timeSigMult = if((measureDur / 0.5 % 2).trunc.asInteger == 0, {measureDur.trunc.asInteger.asString ++ "/4 "},
					{(measureDur / 0.5).trunc.asInteger.asString ++ "/8 "});
				//set time signature for each measure
				timeSig = "\\numericTimeSignature \\time " ++ timeSigMult;
				if(lastTimeSig != timeSig, {lilyString = lilyString ++ timeSig}, {lilyString = lilyString ++ "%{" ++ timeSig ++ "%} "});
				lastTimeSig = timeSig;

				dur = case
				{measureDur == 1} {["4 "]}
				{measureDur == 2} {["2 "]}
				{measureDur == 3} {["2. "]}
				{measureDur == 4} {["1 "]}
				{measureDur == 1.5} {["4. "]}
				{measureDur == 2.5} {["4 ", "4. "]}
				{measureDur == 3.5} {["2 ", "4. "]};

				lilyString = lilyString ++ if(note == "R", {note ++ "1*" ++ timeSigMult},
					{dur.collect({arg d; note ++ d ++ "~ "}).join.drop(-2)});
				lilyString = lilyString ++ if((m < (measureDurs.size - 1)) && (note != "R"), {"~ "}, {""});

				measureIndex = measureIndex + 1;
				beatCountTotal = beatCountTotal + measureDur;

			});

		});

		//write file
		lilyString = lilyString ++ "\\bar \"|.\"\n}";
		lilyFile.write(lilyString);
		lilyFile.close;
	})
}
)
