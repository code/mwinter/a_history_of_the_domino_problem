\version "2.19.81"

genStaff =
#(define-music-function (parser location part)
   (string?) 
   (let * ((file (string-append "includes/ammann_part_" part ".ly")))
   #{
     \new Staff \with {
      instrumentName = #part
      shortInstrumentName = #part
    }
    <<
      \include #file
    >>
   #}))

\paper {
  #(set-paper-size "a4" 'portrait)
  top-margin = 1 \cm
  bottom-margin = 1 \cm
  left-margin = 1.75 \cm
  
  top-system-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))
  
  last-bottom-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))
  
  systems-per-page = 2
  
  print-page-number = ##t
  oddHeaderMarkup = \markup { \on-the-fly #not-first-page "(ammann)" }
  evenHeaderMarkup = \markup { \on-the-fly #not-first-page "(ammann)" }
  oddFooterMarkup = \markup { \fill-line {
    \on-the-fly #not-first-page 
    \concat {
      "-"
      \fontsize #1.5
      \on-the-fly #print-page-number-check-first
      \fromproperty #'page:page-number-string
      "-"}}}
  evenFooterMarkup = \markup { \fill-line {
    \on-the-fly #not-first-page 
    \concat { 
      "-" 
      \fontsize #1.5
      \on-the-fly #print-page-number-check-first
      \fromproperty #'page:page-number-string
      "-"}}}
}

\header {
  title = \markup { \italic {ammann}}
  subtitle = \markup { \normal-text { from \italic{a history of the domino the problem}}}
  composer = \markup \right-column {"michael winter" "(schloss solitude, stuttgart and calle monclova 62, mexico city; 2018-19)"}
  tagline = ""
} 

#(set-global-staff-size 11)

\layout {
  indent = 0.0\cm
  line-width = 17\cm 
  %ragged-last = ##t
  
  \context {
    \Score
      \override BarNumber.extra-offset = #'(0 . 4)
      \override BarNumber.stencil = #(make-stencil-circler 0.1 0.25 ly:text-interface::print)
      \override RehearsalMark #'direction = #DOWN
  }
  \context {
    \Staff
      \override VerticalAxisGroup.staff-staff-spacing =
      #'((basic-distance . 15 )
      (minimum-distance . 15 )
      (padding . 0 )
      (stretchability . 0))
      
      \override TimeSignature.font-size = #2
      \override TimeSignature.break-align-symbol = #'clef
      \override TimeSignature.X-offset =
        #ly:self-alignment-interface::x-aligned-on-self
      \override TimeSignature.self-alignment-X = #LEFT
      %\override TimeSignature.after-line-breaking =
      %  #shift-right-at-line-begin
      \override TimeSignature.Y-offset = #9
      \override TimeSignature.extra-offset = #'(2 . 0)
      
  }
  
  \context {
    \StaffGroup
    \name "SemiStaffGroup"
    \consists "Span_bar_engraver"
    \override SpanBar.stencil =
      #(lambda (grob) 
        (if (string=? (ly:grob-property grob 'glyph-name) "|")
            (set! (ly:grob-property grob 'glyph-name) ""))
        (ly:span-bar::print grob))
  }
  \context {
    \Score
    \accepts SemiStaffGroup
  }
}


\score {
%showLastLength = R1*128
\new Score 
%\with{ proportionalNotationDuration = #(ly:make-moment 1 16) }
  <<
  \new SemiStaffGroup {
    <<
    \new Staff \with {
      instrumentName = #"8"
      shortInstrumentName = #"8"
      midiInstrument = #"clarinet"
    }
    <<
      \include "includes/ammann_part_0.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"7"
      shortInstrumentName = #"7"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/ammann_part_1.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"6"
      shortInstrumentName = #"6"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/ammann_part_2.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"5"
      shortInstrumentName = #"5"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/ammann_part_3.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"4"
      shortInstrumentName = #"4"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/ammann_part_4.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"3"
      shortInstrumentName = #"3"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/ammann_part_5.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"2"
      shortInstrumentName = #"2"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/ammann_part_6.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"1"
      shortInstrumentName = #"1"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/ammann_part_7.ly"
    >>
   
    %\genStaff #"0"
    %\genStaff #"1"
    %\genStaff #"2"
    %\genStaff #"3"
    %\genStaff #"4"
    %\genStaff #"5"
    >>
  }
  >>
  \layout { }
  \midi { }
}
