\version "2.19.81"

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)	     	   
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and 
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 (begin
		   (ly:grob-suicide! v1-rest)
		   (if (ly:grob? v1-dot)
		       (ly:grob-suicide! v1-dot))
		   (ly:grob-set-property! v2-rest 'staff-position 0)
		   (ly:grob-set-property! v2-rest 'direction 0)
		   (ly:rest::y-offset-callback v2-rest)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))

genFull =
#(define-music-function (parser location mod)
   (string?)
   #{
     <<
    \genStaff #mod #"31" ##t
    \genStaff #mod #"29" ##f
    \genStaff #mod #"27" ##f
    \genStaff #mod #"25" ##f
    \genStaff #mod #"23" ##f
    \genStaff #mod #"21" ##f
    \genStaff #mod #"19" ##f
    \genStaff #mod #"17" ##f
    \genStaff #mod #"15" ##f
    \genStaff #mod #"13" ##f
    \genStaff #mod #"11" ##f
    \genStaff #mod #"9" ##f
    \genStaff #mod #"7" ##f
    \genStaff #mod #"5" ##f
    \genStaff #mod #"3" ##f
    \genStaff #mod #"1" ##f
     >>
   #})

genReduced =
#(define-music-function (parser location mod)
   (string?)
   #{
     <<
    \genStaff #mod #"15" ##t
    \genStaff #mod #"13" ##f
    \genStaff #mod #"11" ##f
    \genStaff #mod #"9" ##f
    \genStaff #mod #"7" ##f
    \genStaff #mod #"5" ##f
    \genStaff #mod #"3" ##f
    \genStaff #mod #"1" ##f
     >>
   #})

genStaff =
#(define-music-function (parser location mod part tsig)
   (string? string? boolean?)
   (let * ((file (string-append "includes/jaendel_rao_" mod "_part_" part ".ly")))
     (if tsig
   #{
     \new Staff \with {
      instrumentName = #part
      shortInstrumentName = #part
    }
    <<
      \repeat unfold 67 { \repeat unfold 3 { s1 \noBreak } s1 \break }
      \include #file
    >>
   #}
   
   #{
     \new Staff \with {
      instrumentName = #part
      shortInstrumentName = #part
      \remove "Time_signature_engraver"
    }
    <<
      \repeat unfold 67 { \repeat unfold 3 { s1 \noBreak } s1 \break }
      \include #file
    >>
   #})))

\paper {
  #(set-paper-size "a4" 'portrait)
  top-margin = 1 \cm
  bottom-margin = 1 \cm
  left-margin = 1.75 \cm
  
  top-system-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))
  
  last-bottom-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))
  
  %systems-per-page = 2
  
  print-page-number = ##t
  oddHeaderMarkup = \markup { \on-the-fly #not-first-page "(jaendel-rao)" }
  evenHeaderMarkup = \markup { \on-the-fly #not-first-page "(jaendel-rao)" }
  oddFooterMarkup = \markup { \fill-line {
    \on-the-fly #not-first-page 
    \concat {
      "-"
      \fontsize #1.5
      \on-the-fly #print-page-number-check-first
      \fromproperty #'page:page-number-string
      "-"}}}
  evenFooterMarkup = \markup { \fill-line {
    \on-the-fly #not-first-page 
    \concat { 
      "-" 
      \fontsize #1.5
      \on-the-fly #print-page-number-check-first
      \fromproperty #'page:page-number-string
      "-"}}}
}

\header {
  title = \markup { \italic {jaendel-rao}}
  subtitle = \markup { \normal-text { from \italic{a history of the domino the problem}}}
  composer = \markup \right-column {"michael winter" "(schloss solitude, stuttgart and calle monclova 62, mexico city; 2018-19)"}
  piece = "reduced version"
  tagline = ""
} 

#(set-global-staff-size 11)

\layout {
  indent = 0.0\cm
  line-width = 17\cm 
  %ragged-last = ##t
  
  \context {
    \Score
      \remove "Mark_engraver" 
      \override BarNumber.extra-offset = #'(0 . 4)
      \override BarNumber.stencil = #(make-stencil-circler 0.1 0.25 ly:text-interface::print)
      \override Stem.stemlet-length = #0.75
      \override DynamicTextSpanner.style = #'none
  }
  \context {
    \Staff
      %\consists "Mark_engraver"
      \remove "Dot_column_engraver"
      %\remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \override StaffSymbol.line-count = #1
      \override NoteHead.no-ledgers = ##t
      \override RestCollision.positioning-done = #merge-rests-on-positioning
      %\override RehearsalMark.X-offset = #1
      %\override RehearsalMark.Y-offset = #4
      \override VerticalAxisGroup.default-staff-staff-spacing =
      #'((basic-distance . 16 )
      (minimum-distance . 16 )
      (padding . 0 )
      (stretchability . 0))
      
       \override TimeSignature.font-size = #2
      \override TimeSignature.break-align-symbol = #'clef
      \override TimeSignature.X-offset =
        #ly:self-alignment-interface::x-aligned-on-self
      \override TimeSignature.self-alignment-X = #LEFT
      %\override TimeSignature.after-line-breaking =
      %  #shift-right-at-line-begin
      \override TimeSignature.Y-offset = #9
      \override TimeSignature.extra-offset = #'(2 . 0)
  }
   \context {
     \Voice
     \consists "Dot_column_engraver"
     \consists "Mark_engraver"
     %\override RehearsalMark.X-offset = #1
     %\override RehearsalMark.Y-offset = #4
   } 
}


%showLastLength = R1*128
\new Score 
\with {proportionalNotationDuration = #(ly:make-moment 1 16)}
  <<
    \genReduced #"reduced"
  >>
