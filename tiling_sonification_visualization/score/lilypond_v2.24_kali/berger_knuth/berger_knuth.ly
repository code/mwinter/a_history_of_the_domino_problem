\version "2.24.1"

genStaff =
#(define-music-function (parser location part)
   (string?) 
   (let * ((file (string-append "includes/berger_part_" part ".ly")))
   #{
     \new Staff \with {
      instrumentName = #part
      shortInstrumentName = #part
    }
    <<
      \include #file
    >>
   #}))

\paper {
  #(set-paper-size "a4" 'portrait)
  top-margin = 1 \cm
  bottom-margin = 1 \cm
  left-margin = 1.75 \cm
  
  top-system-spacing =
  #'((basic-distance . 25 )
  (minimum-distance . 25 )
  (padding . 0 )
  (stretchability . 0))
  
  last-bottom-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))
  
  systems-per-page = 4 %was 5

  print-page-number = ##t
  oddHeaderMarkup = \markup { \unless \on-first-page {"(berger-knuth)"}}
  evenHeaderMarkup = \markup { \unless \on-first-page {"(berger-knuth)"}}
  oddFooterMarkup = \markup { \fill-line {
    \concat {
      "-"
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
  evenFooterMarkup = \markup { \fill-line {
    \concat { 
      "-" 
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
}

\header {
  title = \markup { \italic {berger-knuth}}
  subtitle = \markup { \normal-text { from \italic{a history of the domino the problem}}}
  composer = \markup \right-column {"michael winter" "(schloss solitude, stuttgart and calle monclova 62, mexico city; 2018-19)"}
  tagline = ""
} 

#(set-global-staff-size 14) %was 11

\layout {
  %indent = 0.0\cm
  line-width = 17\cm 
  %ragged-last = ##t
  
  \context {
    \Score
      \override BarNumber.extra-offset = #'(0 . 4)
      \override BarNumber.stencil = #(make-stencil-circler 0.1 0.25 ly:text-interface::print)
      \override RehearsalMark.direction = #DOWN
      \override BarNumber.font-size = #2
      rehearsalMarkFormatter = #format-mark-box-numbers
  }
  \context {
    \Staff
      \override VerticalAxisGroup.staff-staff-spacing =
      #'((basic-distance . 14 )
      (minimum-distance . 14 )
      (padding . 0 )
      (stretchability . 0))
      
      \override TimeSignature.font-size = #2
      \override TimeSignature.break-align-symbol = #'clef
      \override TimeSignature.X-offset =
        #ly:self-alignment-interface::x-aligned-on-self
      \override TimeSignature.self-alignment-X = #LEFT
      %\override TimeSignature.after-line-breaking =
      %  #shift-right-at-line-begin
      \override TimeSignature.Y-offset = #9
      \override TimeSignature.extra-offset = #'(2 . 0)
      
  }
  
  \context {
    \StaffGroup
    \name "SemiStaffGroup"
    \consists "Span_bar_engraver"
    \override SpanBar.stencil =
      #(lambda (grob) 
        (if (string=? (ly:grob-property grob 'glyph-name) "|")
            (set! (ly:grob-property grob 'glyph-name) ""))
        (ly:span-bar::print grob))
  }
  \context {
    \Score
    \accepts SemiStaffGroup
  }
}

\score{
%showLastLength = R1*128
\new Score 
%\with{ proportionalNotationDuration = #(ly:make-moment 1 16) }
  <<
  \new SemiStaffGroup {
    <<
    \new Staff \with {
      instrumentName = #"violin (1a)"
      shortInstrumentName = #"vn"
      midiInstrument = #"flute"
    }
    <<
      \include "includes/berger_part_3.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"clarinet (2a)"
      shortInstrumentName = #"cl"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \transpose c d
      \include "includes/berger_part_2.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"cello (3a)"
      shortInstrumentName = #"vc"
      midiInstrument = #"viola"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/berger_part_1.ly"
    >>
    >>
  }
  \new SemiStaffGroup {
    <<
%{
    \new Staff \with {
      instrumentName = #"1b"
      shortInstrumentName = #"1b"
      midiInstrument = #"cello"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/berger_part_7.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"2b"
      shortInstrumentName = #"2b"
      midiInstrument = #"saxophone"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/berger_part_6.ly"
    >>
%}

%{
    \new Staff \with {
      instrumentName = #"1b + 2b"
      shortInstrumentName = #"1b + 2b"
      midiInstrument = #"cello"
      \remove "Time_signature_engraver"
    }
    <<
      \override DynamicLineSpanner.direction = #UP
      \include "includes/berger_part_7.ly"
      \\
      \include "includes/berger_part_6.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"3b"
      shortInstrumentName = #"3b"
      midiInstrument = #"bassoon"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/berger_part_5.ly"
    >>
%}

   
    %\genStaff #"0"
    %\genStaff #"1"
    %\genStaff #"2"
    %\genStaff #"3"
    %\genStaff #"4"
    %\genStaff #"5"
    >>
  }
  >>
  %\midi{}
  \layout{}
}
