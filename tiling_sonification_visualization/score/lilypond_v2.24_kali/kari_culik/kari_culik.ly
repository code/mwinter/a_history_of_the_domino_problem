\version "2.24.1"

genStaff =
#(define-music-function (parser location part)
   (string?) 
   (let * ((file (string-append "includes/kari_culik_part_" part ".ly")))
   #{
     \new Staff \with {
      instrumentName = #part
      shortInstrumentName = #part
    }
    <<
      \include #file
    >>
   #}))

\paper {
  #(set-paper-size "a4" 'portrait)
  top-margin = 1 \cm
  bottom-margin = 1 \cm
  left-margin = 2 \cm
  
  top-system-spacing =
  #'((basic-distance . 25 )
  (minimum-distance . 25 )
  (padding . 0 )
  (stretchability . 0))
  
  last-bottom-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))
  
  systems-per-page = 2
  
  print-page-number = ##t
  oddHeaderMarkup = \markup { \unless \on-first-page {"(kari-culik)"}}
  evenHeaderMarkup = \markup { \unless \on-first-page {"(kari-culik)"}}
  oddFooterMarkup = \markup { \fill-line {
    \concat {
      "-"
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
  evenFooterMarkup = \markup { \fill-line {
    \concat { 
      "-" 
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
}

\header {
  title = \markup { \italic {kari-culik}}
  subtitle = \markup { \normal-text { from \italic{a history of the domino the problem}}}
  composer = \markup \right-column {"michael winter" "(schloss solitude, stuttgart and calle monclova 62, mexico city; 2018-19)"}
  tagline = ""
} 

#(set-global-staff-size 14)

\layout {
  %indent = 0.0\cm
  line-width = 17\cm 
  %ragged-last = ##t
  
  \context {
    \Score
      \override BarNumber.extra-offset = #'(0 . 4)
      \override BarNumber.stencil = #(make-stencil-circler 0.1 0.25 ly:text-interface::print)
      \override RehearsalMark.direction = #DOWN
      \override BarNumber.font-size = #2
      rehearsalMarkFormatter = #format-mark-box-numbers
  }
  \context {
    \Staff
      \override VerticalAxisGroup.staff-staff-spacing =
      #'((basic-distance . 15 )
      (minimum-distance . 15 )
      (padding . 0 )
      (stretchability . 0))
      
      \override TimeSignature.font-size = #2
      \override TimeSignature.break-align-symbol = #'clef
      \override TimeSignature.X-offset =
        #ly:self-alignment-interface::x-aligned-on-self
      \override TimeSignature.self-alignment-X = #LEFT
      %\override TimeSignature.after-line-breaking =
      %  #shift-right-at-line-begin
      \override TimeSignature.Y-offset = #10
      \override TimeSignature.extra-offset = #'(2 . 0)
      
  }
  
  \context {
    \StaffGroup
    \name "SemiStaffGroup"
    \consists "Span_bar_engraver"
    \override SpanBar.stencil =
      #(lambda (grob) 
        (if (string=? (ly:grob-property grob 'glyph-name) "|")
            (set! (ly:grob-property grob 'glyph-name) ""))
        (ly:span-bar::print grob))
  }
  \context {
    \Score
    \accepts SemiStaffGroup
  }
}


%showLastLength = R1*128
\new Score 
\with{ proportionalNotationDuration = #(ly:make-moment 1 8) }
  <<
  \new StaffGroup {
    <<
    \new Staff \with {
      instrumentName = #"cello (ens. 1)"
      shortInstrumentName = #"vc"
    }
    <<
      \include "includes/kari_culik_ensemble_part_0.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"violin (ens. 2)"
      shortInstrumentName = #"vn"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/kari_culik_ensemble_part_1.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"clarinet I (ens. 3)"
      shortInstrumentName = #"clI"
      \remove "Time_signature_engraver"
    }
    <<
      \transpose c d
      \include "includes/kari_culik_ensemble_part_2.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"clarinet II (ens. 4)"
      shortInstrumentName = #"clII"
      \remove "Time_signature_engraver"
    }
    <<
      \transpose c d
      \include "includes/kari_culik_ensemble_part_3.ly"
    >>
    %{
    \new Staff \with {
      instrumentName = #"ensemble 5"
      shortInstrumentName = #"ens. 5"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/kari_culik_ensemble_part_4.ly"
    >>
    %}
      >>
  }
  
  \new StaffGroup {
    <<
    
    \new Staff \with {
      instrumentName = #"synth I (bass)"
      shortInstrumentName = #"synI"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/kari_culik_bass_part.ly"
    >>
    
    \new Staff \with {
      \override StaffSymbol.line-count = #2
      \override StaffSymbol.staff-space = #2
      \override Stem.length = #3
      instrumentName = #"synth II (noise)"
      shortInstrumentName = #"synII"
      \remove "Time_signature_engraver"
    }
    <<
      \include "includes/kari_culik_perc_part.ly"
    >>
   
    %\genStaff #"0"
    %\genStaff #"1"
    %\genStaff #"2"
    %\genStaff #"3"
    %\genStaff #"4"
    %\genStaff #"5"
      >>
  }
  >>
