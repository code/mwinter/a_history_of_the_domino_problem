\version "2.24.1"

forced-accidentals-pitches =
#(music-pitches #{ bes b #})

#(define (diatonic-pitch-class= p q)
   (and (= (ly:pitch-notename p) (ly:pitch-notename q))
        (= (ly:pitch-alteration p) (ly:pitch-alteration q))))

Force_accidentals_engraver =
#(lambda (context)
   (let
    ((ties '())
     (affected-note-events '()))
   (make-engraver
    (end-acknowledgers
     ((tie-interface engraver grob source-engraver)
      (set! ties (cons grob ties))))
    ((stop-translation-timestep engraver)
     (for-each
      (lambda (tie)
        (let* ((right-notehead (ly:spanner-bound tie RIGHT))
               (right-notehead-cause (ly:grob-property right-notehead 'cause)))
          (if (member right-notehead-cause affected-note-events)
              (ly:grob-suicide!
               ;;; the accidental grob should be guaranteed to
               ;;; exist since we forced it into existence...
               (ly:grob-object
                right-notehead 'accidental-grob)))))
      ties)
     (set! ties '())
     (set! affected-note-events '()))
    (listeners
     ((note-event engraver event)
      (when (member (ly:event-property event 'pitch)
                    forced-accidentals-pitches
                    diatonic-pitch-class=)
        (ly:event-set-property! event 'force-accidental #t)
        (set! affected-note-events
              (cons event affected-note-events))))))))

genStaff =
#(define-music-function (parser location part)
   (string?) 
   (let * ((file (string-append "includes/ammann_part_" part ".ly")))
   #{
     \new Staff \with {
      instrumentName = #part
      shortInstrumentName = #part
    }
    <<
      \include #file
    >>
   #}))

transposePitchClasses =
#(define-music-function (scaleA scaleB music) (ly:music? ly:music? ly:music?)
  (let* ((scaleA (ly:music-property scaleA 'elements))
         (scaleB (ly:music-property scaleB 'elements))
         (scaleA (map (lambda (x) (ly:music-property x 'pitch)) scaleA))
         (scaleB (map (lambda (x) (ly:music-property x 'pitch)) scaleB))
         (classesA (map (lambda (p) (cons (ly:pitch-notename p) (ly:pitch-alteration p))) scaleA)))
  (map-some-music
    (lambda (m)
      (let ((p (ly:music-property m 'pitch)))
        (if (not (null? p))
            (let* ((nn (ly:pitch-notename p))
                   (oct (ly:pitch-octave p))
                   (alt (ly:pitch-alteration p))
                   (pos (list-index (lambda (x) (and (= (car x) nn) (= (cdr x) alt))) classesA)))
            (if pos
              (let* ((p2 (list-ref scaleA pos))
                     (oct2 (ly:pitch-octave p2))
                     (p3 (list-ref scaleB pos))
                     (new-pitch (ly:pitch-transpose p3 (ly:make-pitch (- oct oct2) 0))))
              (ly:music-set-property! m 'pitch new-pitch)))
              m)
              #f)))
       music)
     music))

\paper {
  #(set-paper-size "a4" 'portrait)
  top-margin = 1 \cm
  bottom-margin = 1 \cm
  left-margin = 1.75 \cm
  
  top-system-spacing =
  #'((basic-distance . 20 )
  (minimum-distance . 20 )
  (padding . 0 )
  (stretchability . 0))
  
  last-bottom-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))
  
  systems-per-page = 4
  
  print-page-number = ##t
  oddHeaderMarkup = \markup { \unless \on-first-page {"(ammann)"}}
  evenHeaderMarkup = \markup { \unless \on-first-page {"(ammann)"}}
  oddFooterMarkup = \markup { \fill-line {
    \concat {
      "-"
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
  evenFooterMarkup = \markup { \fill-line {
    \concat { 
      "-" 
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
}

\header {
  title = \markup { \italic {ammann}}
  subtitle = \markup { \normal-text { from \italic{a history of the domino the problem}}}
  composer = \markup \right-column {"michael winter" "(schloss solitude, stuttgart and calle monclova 62, mexico city; 2018-19)"}
  tagline = ""
} 

#(set-global-staff-size 14)

\layout {
  %indent = 0.0\cm
  line-width = 17\cm 
  %ragged-last = ##t
  
  \context {
    \Score
      \override BarNumber.extra-offset = #'(0 . 4)
      \override BarNumber.stencil = #(make-stencil-circler 0.1 0.25 ly:text-interface::print)
      \override RehearsalMark.direction = #DOWN
      \override BarNumber.font-size = #2
      rehearsalMarkFormatter = #format-mark-box-numbers
  }
  \context {
    \Staff
      \override VerticalAxisGroup.staff-staff-spacing =
      #'((basic-distance . 15 )
      (minimum-distance . 15 )
      (padding . 0 )
      (stretchability . 0))
      
      \override TimeSignature.font-size = #2
      \override TimeSignature.break-align-symbol = #'clef
      \override TimeSignature.X-offset =
        #ly:self-alignment-interface::x-aligned-on-self
      \override TimeSignature.self-alignment-X = #LEFT
      %\override TimeSignature.after-line-breaking =
      %  #shift-right-at-line-begin
      %\override TimeSignature.Y-offset = #11
      \override TimeSignature.Y-offset = #8
      \override TimeSignature.extra-offset = #'(2 . 0)
      
  }
  
  \context {
    \StaffGroup
    \name "SemiStaffGroup"
    \consists "Span_bar_engraver"
    \override SpanBar.stencil =
      #(lambda (grob) 
        (if (string=? (ly:grob-property grob 'glyph-name) "|")
            (set! (ly:grob-property grob 'glyph-name) ""))
        (ly:span-bar::print grob))
  }
  \context {
    \Score
    \accepts SemiStaffGroup
  }
  \context {
    \Voice
    \consists #Force_accidentals_engraver
  }
}


\score {
%showLastLength = R1*128
\new Score 
%\with{ proportionalNotationDuration = #(ly:make-moment 1 16) }
  <<
  \new SemiStaffGroup {
    <<
    
    %\new Staff \with {
    %  instrumentName = #"synth II (4)"
    %  shortInstrumentName = #"synII"
    %  midiInstrument = #"clarinet"
    %}
    %<<
    %  %\transpose c c'
    %  \transpose a c''
    %  \transposePitchClasses {a b cis dih e fih geh gis} {a b cis dis e f g gis}
    %  \include "includes/ammann_part_4.ly"
    %>>
    
    \new Staff \with {
      %instrumentName = #"synth II (3)"
      %shortInstrumentName = #"synII"
      instrumentName = #"harmonium (3)"
      shortInstrumentName = #"harm"
      midiInstrument = #"clarinet"
      %\remove "Time_signature_engraver"
    }
    <<
      \clef "treble_8"
      %\transpose c c,
      \transpose a c
      \transposePitchClasses {a b cis dih e fih geh gis} {a b cis dis e f g gis}
      \include "includes/ammann_part_5.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"synth I (2)"
      shortInstrumentName = #"synI"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \transpose a c'
      \transposePitchClasses {a b cis dih e fih geh gis} {a b cis dis e f g gis}
      \include "includes/ammann_part_6.ly"
    >>
    
    \new Staff \with {
      instrumentName = #"synth I (1)"
      shortInstrumentName = #"synI"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \clef bass
      %\transpose c c,
      \transpose a c,
      \transposePitchClasses {a b cis dih e fih geh gis} {a b cis dis e f g gis}
      \include "includes/ammann_part_7.ly"
    >>
   
    %\genStaff #"0"
    %\genStaff #"1"
    %\genStaff #"2"
    %\genStaff #"3"
    %\genStaff #"4"
    %\genStaff #"5"
    >>
  }
  >>
  \layout { }
  %\midi { }
}
