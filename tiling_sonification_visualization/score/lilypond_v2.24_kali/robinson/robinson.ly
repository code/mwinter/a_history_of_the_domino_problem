\version "2.24.1"

paren =
#(define-event-function (parser location dyn) (ly:event?)
   (make-dynamic-script
    #{ \markup \concat {
         \normal-text \italic \fontsize #2 <
	 \pad-x #0.2 #(ly:music-property dyn 'text)
	 \normal-text \italic \fontsize #2 >
       }
    #}))

transposeScale =
#(define-music-function (scaleA scaleB music) (ly:music? ly:music? ly:music?)
   (let* ((scaleA (ly:music-property scaleA 'elements))
          (scaleB (ly:music-property scaleB 'elements))
          (scaleA (map (lambda (x) (ly:music-property x 'pitch)) scaleA))
          (scaleB (map (lambda (x) (ly:music-property x 'pitch)) scaleB))
          (rootsA (map ly:pitch-notename scaleA)))
   (map-some-music
    (lambda (m)
      (let ((p (ly:music-property m 'pitch)))
        (if (not (null? p))
            (let* ((nn (ly:pitch-notename p))
                   (oct (ly:pitch-octave p))
                   (alt (ly:pitch-alteration p))
                   (pos (list-index (lambda (x) (= x nn)) rootsA))
                   (p2 (list-ref scaleA pos))
                   (oct2 (ly:pitch-octave p2))
                   (alt2 (ly:pitch-alteration p2))
                   (p3 (list-ref scaleB pos))
                   (new-pitch (ly:pitch-transpose p3 (ly:make-pitch (- oct oct2) 0 (- alt alt2)))))
              (ly:music-set-property! m 'pitch new-pitch)
              m)
            #f)))
    music)
   music))

\paper {
  #(set-paper-size "a4" 'portrait)
  top-margin = 1 \cm
  bottom-margin = 1 \cm
  left-margin = 1.75 \cm
  
  top-system-spacing =
  #'((basic-distance . 25 )
  (minimum-distance . 25 )
  (padding . 0 )
  (stretchability . 0))
  
  last-bottom-spacing =
  #'((basic-distance . 25 )
  (minimum-distance . 25 )
  (padding . 0 )
  (stretchability . 0))
  
  %systems-per-page = 2
  
  print-page-number = ##t
  oddHeaderMarkup = \markup { \unless \on-first-page {"(robinson)"}}
  evenHeaderMarkup = \markup { \unless \on-first-page {"(robinson)"}}
  oddFooterMarkup = \markup { \fill-line {
    \concat {
      "-"
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
  evenFooterMarkup = \markup { \fill-line {
    \concat { 
      "-" 
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
}

\header {
  title = \markup { \italic {robinson}}
  subtitle = \markup { \normal-text { from \italic{a history of the domino the problem}}}
  composer = \markup \right-column {"michael winter" "(schloss solitude, stuttgart and calle monclova 62, mexico city; 2018-19)"}
  tagline = ""
} 

#(set-global-staff-size 14)

\layout {
  %indent = 0.0\cm
  line-width = 17\cm 
  %ragged-last = ##t
  
  \context {
    \Score
      %\remove "Mark_engraver" 
      %\override BarNumber.extra-offset = #'(0 . 4)
      \override BarNumber.stencil = #(make-stencil-circler 0.1 0.25 ly:text-interface::print)
      \override Stem.stemlet-length = #0.75
      \override BarNumber.font-size = #2
      %\override DynamicTextSpanner.style = #'none
      rehearsalMarkFormatter = #format-mark-box-numbers
  }
  \context {
    \Staff
      %\consists "Mark_engraver"
      %\remove "Dot_column_engraver"
      %\remove "Time_signature_engraver"
      %\remove "Clef_engraver"
      %\override StaffSymbol.line-count = #1
      %\override NoteHead.no-ledgers = ##t
      %\override RestCollision.positioning-done = #merge-rests-on-positioning
     \override VerticalAxisGroup.staff-staff-spacing =
      #'((basic-distance . 14 )
      (minimum-distance . 14 )
      (padding . 0 )
      (stretchability . 0))
      
      \override RehearsalMark.X-offset = #1
      \override RehearsalMark.Y-offset = #4
      \override VerticalAxisGroup.default-staff-staff-spacing =
      #'((basic-distance . 16 )
      (minimum-distance . 16 )
      (padding . 0 )
      (stretchability . 0))
      
      \override TimeSignature.font-size = #2
      \override TimeSignature.break-align-symbol = #'clef
      \override TimeSignature.X-offset =
        #ly:self-alignment-interface::x-aligned-on-self
      \override TimeSignature.self-alignment-X = #LEFT
      %\override TimeSignature.after-line-breaking =
      %  #shift-right-at-line-begin
      \override TimeSignature.Y-offset = #9
      \override TimeSignature.extra-offset = #'(2 . 0)
  }
  \context {
    \StaffGroup
    \name "SemiStaffGroup"
    \consists "Span_bar_engraver"
    \override SpanBar.stencil =
      #(lambda (grob) 
        (if (string=? (ly:grob-property grob 'glyph-name) "|")
            (set! (ly:grob-property grob 'glyph-name) ""))
        (ly:span-bar::print grob))
  }
  \context {
    \Score
    \accepts SemiStaffGroup
  }
}

\midi { }


%showLastLength = R1*128
\score{
\new Score 
  <<
    \new SemiStaffGroup {
    <<
    %{
    \new Staff \with {
      instrumentName = "1"
      shortInstrumentName = "1"
      midiInstrument = #"flute"
    }
    <<
      \numericTimeSignature \clef "treble" \include "includes/robinson_part_8.ly"
    >>
    
    \new Staff \with {
      instrumentName = "2"
      shortInstrumentName = "2"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \numericTimeSignature \include "includes/robinson_part_7.ly"
    >>
    %}
    \new Staff \with {
      instrumentName = "clarinet I (3)"
      shortInstrumentName = "clI"
      midiInstrument = #"clarinet"
      \remove "Time_signature_engraver"
    }
    <<
      \numericTimeSignature  
      \transpose c d 
      \include "includes/robinson_part_6.ly"
    >>
    
    \new Staff \with {
      instrumentName = "synth (4)"
      shortInstrumentName = "syn"
      midiInstrument = #"cello"
      \remove "Time_signature_engraver"
    }
    <<
      \numericTimeSignature 
      \transposeScale {c d e fih g aeh aih} {c d e fis g a bes}
      \include "includes/robinson_part_5.ly"
    >>
    
     \new Staff \with {
      instrumentName = "cello (5)"
      shortInstrumentName = "vc"
      midiInstrument = #"cello"
      \remove "Time_signature_engraver"
    }
    <<
      \numericTimeSignature \include "includes/robinson_part_4.ly"
    >>
    
     \new Staff \with {
      instrumentName = "violin (6)"
      shortInstrumentName = "vn"
      midiInstrument = #"cello"
      \remove "Time_signature_engraver"
    }
    <<
      \numericTimeSignature 
      %\clef "bass" 
      \include "includes/robinson_part_3.ly"
    >>
    
     \new Staff \with {
      instrumentName = "harmonium (7)"
      shortInstrumentName = "harm"
      midiInstrument = #"cello"
      \remove "Time_signature_engraver"
    }
    <<
      \numericTimeSignature 
      \clef "bass" 
      \transposeScale {c d e fih g aeh aih} {c d e fis g a bes}
      \include "includes/robinson_part_2.ly"
    >>
    
     \new Staff \with {
      instrumentName = "clarinet II (8)"
      shortInstrumentName = "clII"
      midiInstrument = #"cello"
      \remove "Time_signature_engraver"
    }
    <<
      \numericTimeSignature 
      %\clef "bass" 
      \transpose c d
      \include "includes/robinson_part_1.ly"
    >>
    >>
    }
  >>
  
  \layout{}
  %\midi{}
}