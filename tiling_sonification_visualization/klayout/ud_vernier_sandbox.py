import pya
import os

base_dir = os.path.dirname(os.path.abspath(__file__))

# init vars
velocity_ratio1 = 1
opening1 = 4000

velocity_ratio2 = 5
opening2 = opening1 * velocity_ratio1 / velocity_ratio2 


pixel_size = 20
shift_mult = 5
image_size = 4266 * pixel_size * 100 #calculate this directly (4266 and 4242 for shift_mult 5 and 3, respectively)
image_size_half = image_size / 2
image_dist = (shift_mult * 3 * 2 + 2) * pixel_size * 100
object_border = 0 
#print(image_dist)


# create layout
layout = pya.Layout()
layout.dbu = 0.01
top = layout.create_cell("Top")
layer1_index = layout.insert_layer(pya.LayerInfo.new(1, 0))
layer2_index = layout.insert_layer(pya.LayerInfo.new(2, 0))
layer3_index = layout.insert_layer(pya.LayerInfo.new(3, 0))
layer4_index = layout.insert_layer(pya.LayerInfo.new(4, 0))
layer5_index = layout.insert_layer(pya.LayerInfo.new(5, 0))

# verniers
linear_grating_vernier = layout.create_cell("Vernier")
base_period = image_size_half - 500000
revealing_period = image_dist
pitch11 = (base_period * (revealing_period / velocity_ratio1)) / (base_period - (revealing_period / velocity_ratio1))
pitch12 = revealing_period / velocity_ratio1
#print(pitch11)
#print(pitch12)


frame_width = 300000

for i in range(-1 * int((image_size_half) / pitch11), int((image_size_half) / pitch11)):
  line = layout.create_cell("Line")
  line.shapes(layer2_index).insert(pya.Box(opening1 / 2, -1 * frame_width, pitch11 - opening1 / 2, 0))
  linear_grating_vernier.insert(pya.CellInstArray(line.cell_index(), pya.Trans(i * pitch11, 0)))
  
for i in range(-1 * int((image_size_half - image_dist) / pitch12), int((image_size_half - image_dist) / pitch12)):
  line = layout.create_cell("Line")
  line.shapes(layer1_index).insert(pya.Box(opening1 / 2, -1 * frame_width, pitch12 - opening1 / 2, 0))
  linear_grating_vernier.insert(pya.CellInstArray(line.cell_index(), pya.Trans(i * pitch12, 0)))

base_period = -1 * (image_size_half - 500000)
revealing_period = image_dist
pitch21 = (base_period * (revealing_period / velocity_ratio2)) / (base_period - (revealing_period / velocity_ratio2))
pitch22 = revealing_period / velocity_ratio2
  
for i in range(-1 * int(image_size_half / pitch21), int(image_size_half / pitch21)):
  line = layout.create_cell("Line")
  line.shapes(layer2_index).insert(pya.Box(opening2 / 2, 0, pitch21 - opening2 / 2, frame_width))
  linear_grating_vernier.insert(pya.CellInstArray(line.cell_index(), pya.Trans(i * pitch21, 0)))
  

for i in range(-1 * int((image_size_half - image_dist) / pitch22), int((image_size_half - image_dist) / pitch22)):
  line = layout.create_cell("Line")
  line.shapes(layer1_index).insert(pya.Box(opening2 / 2, 0, pitch22 - opening2 / 2, frame_width))
  linear_grating_vernier.insert(pya.CellInstArray(line.cell_index(), pya.Trans(i * pitch22, 0)))

line = layout.create_cell("Line")
line.shapes(layer2_index).insert(pya.Box(image_size_half - (2 * image_dist), -1 * frame_width, image_size_half, 0))
line.shapes(layer2_index).insert(pya.Box(-1 * (image_size_half - (2 * image_dist)), -1 * frame_width, -1 * (image_size_half), 0))
line.shapes(layer2_index).insert(pya.Box(image_size_half - (2 * image_dist), image_dist, image_size_half, image_dist + frame_width))
line.shapes(layer2_index).insert(pya.Box(-1 * (image_size_half - (2 * image_dist)), image_dist, -1 * (image_size_half), image_dist + frame_width))

#These four lines can be taken away to get rid of the bounding black out
line.shapes(layer2_index).insert(pya.Box(-1 * image_size_half, image_dist, image_size_half, 0 - image_dist))
line.shapes(layer2_index).insert(pya.Box(-1 * image_size_half, -1 * (frame_width - image_dist), image_size_half, -1 * (frame_width + image_dist)))
line.shapes(layer2_index).insert(pya.Box(-1 * image_size_half, 0, image_size_half, image_dist))
line.shapes(layer2_index).insert(pya.Box(-1 * image_size_half, frame_width + (2 * image_dist), image_size_half, frame_width - image_dist))
linear_grating_vernier.insert(pya.CellInstArray(line.cell_index(), pya.Trans(0, 0)))

  
line = layout.create_cell("Line")
line.shapes(layer1_index).insert(pya.Box(image_size_half - image_dist, -1 * frame_width, image_size_half - (2 * image_dist), 0))
line.shapes(layer1_index).insert(pya.Box(-1 * (image_size_half - image_dist), -1 * frame_width, -1 * (image_size_half - (2 * image_dist)), 0))
line.shapes(layer1_index).insert(pya.Box(image_size_half - image_dist, 0, image_size_half - (2 * image_dist), frame_width))
line.shapes(layer1_index).insert(pya.Box(-1 * (image_size_half - image_dist), 0, -1 * (image_size_half - (2 * image_dist)), frame_width))
linear_grating_vernier.insert(pya.CellInstArray(line.cell_index(), pya.Trans(0, 0)))

  
top.insert(pya.CellInstArray(linear_grating_vernier.cell_index(), pya.Trans(0,0 )))

top.flatten(1)

top.write(os.path.join(base_dir, "..", "gds", "ud_vernier_test.gds"))

bb_region = pya.Region(pya.Box(-5000000, -1 * (frame_width + image_dist), 5000000, frame_width + (2 * image_dist)))
wafer1 = pya.Region(top.begin_shapes_rec(layer1_index))
wafer2 = pya.Region(top.begin_shapes_rec(layer2_index))


alignment_inv_layout = pya.Layout()
alignment_inv = alignment_inv_layout.create_cell("Alignment_Inverse")
alignment_inv_layout.dbu = 0.01
layer1_index = alignment_inv_layout.insert_layer(pya.LayerInfo.new(1, 0))
layer2_index = alignment_inv_layout.insert_layer(pya.LayerInfo.new(2, 0))

alignment_inv.shapes(layer1_index).insert(bb_region - wafer1) 
alignment_inv.shapes(layer2_index).insert(bb_region - wafer2) 

alignment_inv_layout.write(os.path.join(base_dir, "..", "gds", "inverted_tonality", "ud_vernier_test_inverse.gds"))



