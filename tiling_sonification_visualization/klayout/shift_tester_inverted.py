import pya

#amount to shift in units of distance between images
shift_x = 0
shift_y = 0.1

#vars on current sizes
shift_mult = 5
pixel_size = 20
image_dist = (shift_mult * 3 * 2 + 2) * pixel_size * 100
print(image_dist)

#shift and take the overlap returning a 3rd layer
ly = pya.CellView.active().layout()
l10 = ly.layer(1, 0)
l20 = ly.layer(2, 0)
r10 = pya.Region(ly.top_cell().begin_shapes_rec(l10))
r20 = pya.Region(ly.top_cell().begin_shapes_rec(l20))
r10.move(image_dist * shift_x, image_dist* shift_y)
ly.delete_layer(ly.layer(3, 0))
ly.top_cell().shapes(ly.layer(3, 0)).insert(r10 & r20)
pya.LayoutView.current().add_missing_layers()