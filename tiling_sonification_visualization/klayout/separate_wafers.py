import pya
import os

base_dir = os.path.dirname(os.path.abspath(__file__))

layout = pya.Layout()
layout.read(os.path.join(base_dir, "..", "gds", "image_with_alignment_marks_overlapped.gds"))

layout.delete_layer(1)
layer3_index = layout.insert_layer(pya.LayerInfo.new(3, 0))
layout.top_cell().shapes(0).insert(pya.Box(-7500000, 6850000 - 1850000 + 30000, 7500000, 6850000 - 1850000 + 30000 + 5000))
layout.top_cell().shapes(layer3_index).insert(pya.Box(-7500000, -7500000, 7500000, 7500000))
#layout.transform(pya.Trans(2, False, 0, 0))

layout.write(os.path.join(base_dir, "..", "gds", "wafer_1.gds"))


layout = pya.Layout()
layout.read(os.path.join(base_dir, "..", "gds", "image_with_alignment_marks_overlapped.gds"))

layout.delete_layer(0)
layer3_index = layout.insert_layer(pya.LayerInfo.new(3, 0))
#layout.transform(pya.Trans(2, True, 0, 0))
layout.transform(pya.Trans(0, True, 0, 0))
layout.top_cell().shapes(1).insert(pya.Box(-7500000, 6850000 - 1850000 + 30000, 7500000, 6850000 - 1850000 + 30000 + 5000))
layout.top_cell().shapes(layer3_index).insert(pya.Box(-7500000, -7500000, 7500000, 7500000))


layout.write(os.path.join(base_dir,  "..", "gds", "wafer_2.gds"))


layout = pya.Layout()
layout.read(os.path.join(base_dir, "..", "gds", "inverted_tonality", "image_with_alignment_marks_overlapped_inverse.gds"))

layout.delete_layer(1)
#layout.transform(pya.Trans(2, False, 0, 0))

layout.write(os.path.join(base_dir, "..", "gds", "inverted_tonality", "wafer_1_inverse.gds"))


layout = pya.Layout()
layout.read(os.path.join(base_dir, "..", "gds", "inverted_tonality", "image_with_alignment_marks_overlapped_inverse.gds"))

layout.delete_layer(0)
#layout.transform(pya.Trans(2, True, 0, 0))
layout.transform(pya.Trans(0, True, 0, 0))

layout.write(os.path.join(base_dir, "..", "gds", "inverted_tonality", "wafer_2_inverse.gds"))
