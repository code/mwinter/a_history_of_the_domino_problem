import pya
import os

base_dir = os.path.dirname(os.path.abspath(__file__))

ly1 = pya.Layout()
ly1.read(os.path.join(base_dir, "..", "gds", "alignment_marks_overlapped.gds"))

ly2 = pya.Layout()
ly2.read(os.path.join(base_dir, "..", "gds", "image_overlapped.gds"))

ly1_top_cell = ly1.top_cell()
tmp = ly1.create_cell("Image")
tmp.copy_tree(ly2.top_cell())

ly1_top_cell.insert(pya.CellInstArray(tmp.cell_index(), pya.Trans(3, False, 0, 0)))
ly1.rename_cell(ly1_top_cell.cell_index(), "All")
ly1_top_cell.flatten(1)
 
ly1.write(os.path.join(base_dir, "..", "gds", "image_with_alignment_marks_overlapped.gds"))



ly1 = pya.Layout()
ly1.read(os.path.join(base_dir, "..", "gds", "inverted_tonality", "alignment_marks_overlapped_inverse.gds"))

ly2 = pya.Layout()
ly2.read(os.path.join(base_dir, "..", "gds", "inverted_tonality", "image_overlapped_inverse.gds"))

ly2.top_cell().transform_into(pya.Trans(3, False, 0, 0))

processor = pya.ShapeProcessor()
processor.boolean(ly1, ly1.top_cell(), 0, ly2, ly2.top_cell(), 0, ly1.top_cell().shapes(0), 1, False, False, True)

processor = pya.ShapeProcessor()
processor.boolean(ly1, ly1.top_cell(), 1, ly2, ly2.top_cell(), 1, ly1.top_cell().shapes(1), 1, False, False, True)

ly1.write(os.path.join(base_dir, "..", "gds",  "inverted_tonality", "image_with_alignment_marks_overlapped_inverse.gds"))
